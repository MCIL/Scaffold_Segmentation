This is the program code that accompanies our paper:
Forton SM, Latourette MT, Parys M, Kiupel M, Shahriari D, Sakamoto JS, Shapiro EM. In Vivo Microcomputed Tomography of Nanocrystal-Doped Tissue Engineered Scaffolds, ACS Biomaterials Science & Engineering, 2(4):508-516, 2016
http://pubs.acs.org/doi/abs/10.1021/acsbiomaterials.5b00476

The program performs segmentation of micro CT images to separate nanocrystal-doped scaffolds from background.

Requirements:
This program requires MATLAB and the vfftools package to convert the native VFF file format produced by the Locus RS 80 micro CT system. Mathematica and the MicroCtTools package are required to perform the segmentations and visualize the results.